import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  TextInput,
} from 'react-native';
import moment from 'moment';
import DatePicker from 'react-native-datepicker';
import {createFood} from './repository/foodRepository';

const CreateScreen = ({navigation}) => {
  const [name, setName] = useState('');
  const [weight, setWeight] = useState('');
  const [date, setDate] = useState(moment(new Date()).format('YYYY/MM/DD'));
  const [value, setValue] = useState('');

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.inputContainer}>
        <Text
          style={{
            fontSize: 30,
            textAlign: 'center',
            marginBottom: 20,
          }}>
          Create Food
        </Text>
        <TextInput
          style={styles.input}
          placeholder="name"
          value={name}
          onChangeText={setName}
        />
        <TextInput
          style={styles.input}
          placeholder="weight (kg)"
          value={weight}
          onChangeText={setWeight}
        />
        <TextInput
          style={styles.input}
          placeholder="Price"
          value={value}
          onChangeText={setValue}
        />
        <DatePicker
          style={{width: 200}}
          date={date}
          mode="date"
          placeholder="select date"
          format="YYYY/MM/DD"
          confirmBtnText="Confirm"
          cancelBtnText="Cancel"
          onDateChange={date => {
            setDate(date);
          }}
        />
        <TouchableOpacity
          style={styles.button}
          onPress={async () => {
            {
              console.log(value);
              await createFood(name, weight, date, value),
                navigation.reset({index: 1, routes: [{name: 'FoodList'}]});
            }
          }}>
          <Text style={styles.buttonText}>Create</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ccc',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    borderWidth: 1,
    borderColor: '#000',
    // #F15D7B
    borderRadius: 30,
    padding: 10,
    marginVertical: 10,
  },
  inputContainer: {
    justifyContent: 'center',
    backgroundColor: '#fff',
    padding: 20,
    borderRadius: 20,
    textAlign: 'center',
    width: '90%',
    height: '90%',
  },
  button: {
    textAlign: 'center',
    textAlignVertical: 'center',
    backgroundColor: '#F15D7B',
    borderRadius: 30,
    marginTop: 25,
  },
  buttonText: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: '#fff',
    padding: 10,
  },
});
export default CreateScreen;
