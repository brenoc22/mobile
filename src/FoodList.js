import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  FlatList,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {Card} from './components/Card';
import api from './services/api';
import {TOKEN_KEY, USER_ID_KEY} from './utils/constants';
import authRepository from './repository/authRepository';
import {getFoods} from './repository/foodRepository';
import moment from 'moment';
import MonthPicker from 'react-native-month-year-picker';

const FoodList = ({navigation}) => {
  const [userFoodList, setFoodList] = useState([]);
  const [show, setShow] = useState(false);
  const [date, setDate] = useState(moment().toDate());
  const onValueChange = (event, newDate) => {
    const selectedDate = newDate || date;

    showPicker(false);
    setDate(selectedDate);
  };

  const showPicker = show => {
    setShow(show);
  };

  useEffect(() => {
    getFoods(date).then(res => {
      setFoodList(res.dogfood);
    });
  }, []);

  return (
    <SafeAreaView style={styles.sav}>
      {/* <View>
        <Text>{moment(date, 'MM-YYYY').format('MM/YYYY')}</Text>
        <TouchableOpacity onPress={() => showPicker(true)}>
          <Text>OPEN</Text>
        </TouchableOpacity>
        {show && (
          <MonthPicker
            onChange={onValueChange}
            value={date}
            minimumDate={new Date(2000, 1)}
            maximumDate={new Date(2025, 5)}
            locale="en"
          />
        )}
      </View> */}
      <FlatList
        data={userFoodList}
        renderItem={({item}) => {
          return (
            <Card
              name={item.name}
              weight={item.weight}
              boughtAt={item.createdAt}
              value={item.value}
              _id={item._id}
              onDeleteFood={async () => {
                let refreshedFoodList = await getFoods();
                setFoodList(refreshedFoodList.dogfood);
              }}
            />
          );
        }}
        keyExtractor={item => {
          return item._id;
        }}
      />
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          navigation.push('CreateScreen');
        }}>
        <Text style={styles.btnText}>+</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  sav: {flex: 1},
  button: {
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: '#ee6e73',
    position: 'absolute',
    bottom: 10,
    right: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnText: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 25,
  },
});
export default FoodList;
