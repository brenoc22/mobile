import axios from 'axios';
import {BASE_URL} from '../utils/constants';

const api = axios.create({
  baseURL: 'http://10.0.2.2:3000/',
});

export default api;
