import api from '../services/api';
import {useAsyncStorage} from '@react-native-async-storage/async-storage';
import {TOKEN_KEY, USER_ID_KEY} from '../utils/constants';

let createToken = async token => {
  await useAsyncStorage(TOKEN_KEY).setItem(`Bearer ${token}`);
};
let createUserId = async id => {
  await useAsyncStorage(USER_ID_KEY).setItem(id);
};

let getUserId = async () => {
  return await useAsyncStorage(USER_ID_KEY).getItem();
};
let getToken = async () => {
  return await useAsyncStorage(TOKEN_KEY).getItem();
};
const removeToken = async ()=> {
  return await useAsyncStorage(TOKEN_KEY).removeItem()
}
const removeId = async ()=> {
  return await useAsyncStorage(USER_ID_KEY).removeItem()
}

let login = async (email, password) => {
  try {
    const user = await api.post('auth/authenticate', {
      email,
      password,
    });

    await createToken(user.data.token);
    console.log(user.data.token);
    return user;
  } catch (error) {
    console.log(error);
  }
};
let register = async (name, email, password) => {
  try {
    const user = await api.post('auth/register', {
      name,
      email,
      password,
    });
    createUserId(user.data._id);
    console.log(USER_ID_KEY);
    return user;
  } catch (error) {
    console.log(error);
  }
};
export {login, register, getToken, getUserId};
