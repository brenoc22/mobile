import api from '../services/api';
import {getToken, getUserId} from './authRepository';
import moment from 'moment';

export const getFoods = async date => {
  console.log('FUNÇÃO TOKEN', getToken());
  const config = {headers: {Authorization: await getToken()}};
  const id = await getUserId();
  try {
    const result = await api.get('DF/' + id, config, {
      params: {filterDate: moment(date).format('MM-YYYYY')},
    });
    console.log(result.data);
    return result.data;
  } catch (error) {
    console.log(error);
  }
};
export const createFood = async (name, weight, createdAt, value) => {
  const config = {headers: {Authorization: await getToken()}};
  try {
    await api.post(
      'DF/',
      {
        name: name,
        weight: weight,
        createdAt: createdAt,
        value: value,
      },
      config,
    );
  } catch (error) {
    console.log(error);
  }
};

export const deleteFood = async id => {
  const config = {headers: {Authorization: await getToken()}};
  try {
    await api.delete('DF/' + id, config);
  } catch (error) {
    console.log(error);
  }
};
