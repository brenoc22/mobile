import React, {useState} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Text,
} from 'react-native';
import {login} from './repository/authRepository';

const Login = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.inputContainer}>
        <Text
          style={{
            fontSize: 30,
            textAlign: 'center',
            marginBottom: 20,
          }}>
          Login
        </Text>
        <TextInput
          style={styles.input}
          placeholder="email"
          value={email}
          onChangeText={setEmail}
        />
        <TextInput
          secureTextEntry={true}
          style={styles.input}
          placeholder="password"
          value={password}
          onChangeText={setPassword}
        />
        <TouchableOpacity onPress={() => navigation.push('Register')}>
          <Text>Register</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.button}
          onPress={async () => {
            await login(email, password);
            navigation.reset({index: 0, routes: [{name: 'FoodList'}]});
          }}>
          <Text style={styles.buttonText}>ENTER</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ccc',
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    borderWidth: 1,
    borderColor: '#000',
    // #F15D7B
    borderRadius: 30,
    padding: 10,
    marginVertical: 10,
  },
  inputContainer: {
    justifyContent: 'center',
    backgroundColor: '#fff',
    padding: 20,
    borderRadius: 20,
    textAlign: 'center',
    width: '90%',
    height: '90%',
  },
  button: {
    textAlign: 'center',
    textAlignVertical: 'center',
    backgroundColor: '#F15D7B',
    borderRadius: 30,
    marginTop: 25,
  },
  buttonText: {
    textAlign: 'center',
    fontWeight: 'bold',
    color: '#fff',
    padding: 10,
  },
});

export default Login;
