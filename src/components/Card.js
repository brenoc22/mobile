import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Alert} from 'react-native';
import {deleteFood} from '../repository/foodRepository';
import moment from 'moment';
import Trash from '../assets/trash.svg';
export default function Card(props) {
  return (
    <View style={styles.container}>
      <View style={styles.contentContainer}>
        <Text style={styles.text}>Name: {props.name}</Text>
        <Text style={styles.text}>Weight(kg): {props.weight}</Text>
        <Text style={styles.text}>
          Bought At: {moment(props.boughtAt).format('DD/MM/YYYY')}
        </Text>
        <Text style={styles.text}>Price: {props.value}</Text>
        <TouchableOpacity
          style={styles.delButton}
          onPress={async () => {
            await deleteFood(props._id);
            props.onDeleteFood();
            Alert.alert('Deleted');
          }}>
          <Trash />
        </TouchableOpacity>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    backgroundColor: '#fff',
    padding: 20,
    margin: 10,
  },
  contentContainer: {},
  text: {
    fontWeight: 'bold',
    fontSize: 14,
  },
  delButton: {
    position: 'absolute',
    right: -115,
    top: -10,
  },
});

export {Card};
