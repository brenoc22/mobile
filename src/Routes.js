import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Login from './Login';
import Register from './Register';
import FoodList from './FoodList';
import CreateScreen from './CreateScreen';
const Stack = createNativeStackNavigator();
const Routes = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen
          name="Login"
          component={Login}
          options={{title: 'Zeus'}}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{title: 'Zeus'}}
        />
        <Stack.Screen
          name="FoodList"
          component={FoodList}
          options={{title: 'Zeus'}}
        />
        <Stack.Screen
          name="CreateScreen"
          component={CreateScreen}
          options={{title: 'Zeus'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
